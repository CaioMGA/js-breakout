function createBat(){
	bat = {
		"rect":createRect(280, 300, 80, 10),
		"speed": 5,
		"color": "#DD0000",
		"draw": function(ctx){
			ctx.fillStyle = "#DD0000";
			ctx.fillRect(this.rect.x, this.rect.y, this.rect.w, this.rect.h);
		},
		"move": function (dir){
			this.rect.x += this.speed * dir;

			if(this.rect.x < 0){
				this.rect.x = 0;
			} else if(this.rect.x > canvas.width - this.rect.w){
				this.rect.x = canvas.width - this.rect.w;
			}
		}
	};
	return bat;
}
