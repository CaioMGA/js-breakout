var canvas;
var ctx;
var ball;
var bat;
var left_key_down  = false;
var right_key_down  = false;
var wall = [];
var brickDimensions = {"w":32, "h":16};
var brickOffset = {"x":2, "y":2};
var wallOffset = {"x":30, "y":20};
var brickCount = 0;

function init(){
	canvas = document.getElementById("gameCanvas");
	ctx = canvas.getContext("2d");
	ball = createBall();
	bat = createBat();

	window.onkeydown = function (e) {
		var keyCode = e.keyCode ? e.keyCode : e.which;
		if(keyCode == 37 || keyCode == 65){
			left_key_down = true;
		}
		if(keyCode == 39 || keyCode == 68){
			right_key_down = true;
		}
	};

	window.onkeyup = function (e) {
		var keyCode = e.keyCode ? e.keyCode : e.which;
		if(keyCode == 37 || keyCode == 65){
			left_key_down = false;
		}
		if(keyCode == 39 || keyCode == 68){
			right_key_down = false;
		}
	};

	createBlocks();
	window.setInterval(update, 1000/60);
}

function update(){
	var dir = 0;
	if(left_key_down){
		dir -= 1;
	}
	if(right_key_down){
		dir += 1;
	}
	bat.move(dir);
	ball.move();

	if(testCollision(ball.rect, bat.rect)){
		//ball.yDir *= -1;
		ball.calculateDirection(bat.rect);
		//console.log(ball.yDir);
	}

	brickTest = testBallWallCollision();
	if(brickTest > -1){
		ball.yDir *= -1;
		wall.splice(brickTest, 1); //removes brick at position brickTest
		brickCount--;
	}

	draw();
}

function draw(){
	clearCanvas();
	ball.draw(ctx);
	bat.draw(ctx);
	drawBlocks(ctx);
}

function clearCanvas(){
	ctx.fillStyle = "#000000";
	ctx.fillRect(0, 0, canvas.width, canvas.height);
}

function testCollision(rect1, rect2){
	if (rect1.x < rect2.x + rect2.w &&
 		rect1.x + rect1.w > rect2.x &&
   		rect1.y < rect2.y + rect2.h &&
   		rect1.h + rect1.y > rect2.y) {
    	
    	return true;
	}
	return false;
}

function createRect(x, y, w, h){
	rect = {
		"x":x,
		"y":y,
		"w":w,
		"h":h
	};

	return rect;
}

function createBlocks(){
	for(var i = 0; i < 4; i++){
		for(var j = 0; j < 17; j++){
			var brickPos = {
				"x":((j * brickDimensions.w) + (brickOffset.x * j) + wallOffset.x),
				"y":((i * brickDimensions.h) + (brickOffset.y * i) + wallOffset.y)
			};
			
			wall.push(brickPos);
			brickCount += 1;
		}
	}
}

function drawBlocks(ctx){
	for(var i = 0; i < brickCount; i++){

		ctx.fillStyle = "#99" + (wall[i].y - 20) + (wall[i].y - 20) ;
		ctx.fillRect(wall[i].x, wall[i].y, brickDimensions.w, brickDimensions.h);
	}
}

function testBallWallCollision(){
	//returns brick index if ball collides with a brick
	//returns -1 if ball hits no brick

	if(ball.rect.y > 90){ //ball is too low
		return -1;
	}
	for(var i = 0; i < brickCount; i++){
		r = createRect(wall[i].x, wall[i].y, brickDimensions.w, brickDimensions.h);
		if(testCollision(ball.rect, r)){
			return i;
		}
	}

	return -1;
}


