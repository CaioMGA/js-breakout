function createBall(){
	ball = {
		"rect":createRect(0, 280, 10, 10),
		"xDir": 4,
		"yDir": -4,
		"color": "#DD0000",
		"draw": function(ctx){
			ctx.fillStyle = "#DD0000";
			//ctx.fillStyle = color;
			ctx.fillRect(this.rect.x, this.rect.y, this.rect.w, this.rect.h);
			//console.log("ball draw");
		},
		"move": function (){
			this.rect.x += this.xDir;
			this.rect.y += this.yDir;

			if(this.rect.x < 0){
				this.rect.x = 0;
				this.xDir = -this.xDir;
			} else if(this.rect.x > canvas.width - this.rect.w){
				this.rect.x = canvas.width - this.rect.w;
				this.xDir = -this.xDir;
			}

			if(this.rect.y < 0){
				this.rect.y = 0;
				this.yDir = -this.yDir;
			} else if(this.rect.y > canvas.height - this.rect.h){
				this.rect.y = canvas.height - this.rect.h;
				this.yDir = -this.yDir;
			}
		},
		"calculateDirection": function (batRect){

			var batCenter = batRect.x + (batRect.w / 2);
			var ballCenter = this.rect.x + (this.rect.w / 2);
			var maxPos = batRect.x + batRect.w - (this.rect.w / 2);
			//y = mx + b
			this.xDir = (1 / batCenter / 2) * ballCenter - (1 / batCenter / 2 * batCenter);
			this.xDir *= 80;

			if(this.xDir > 5){
				this.xDir = 5;
			}

			if(this.xDir < -5){
				this.xDir = -5;
			}
			this.yDir = -7 + Math.abs(this.xDir);

		}
	};
	return ball;
}
